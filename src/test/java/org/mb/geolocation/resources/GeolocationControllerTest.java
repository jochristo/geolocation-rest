/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mb.geolocation.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Sets;
import java.io.InputStream;
import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ws.rs.core.Response;
import org.apache.cxf.jaxrs.client.WebClient;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mb.geolocation.api.domain.RootObjectLatLong;
import org.mb.geolocation.api.domain.RootObjectTransactions;
import org.mb.geolocation.api.domain.Transaction;
import org.mb.geolocation.api.resources.GeolocationController;
import org.mb.geolocation.api.resources.ImageController;
import org.mb.geolocation.core.Randomness;

/**
 *
 * @author Admin
 */
@RunWith(Arquillian.class)
public class GeolocationControllerTest
{
    
    private final ObjectMapper mapper = new ObjectMapper();
    /**
     * ShrinkWrap is used to create a war file on the fly.
     *
     * The API is quite expressive and can build any possible
     * flavor of war file.  It can quite easily return a rebuilt
     * war file as well.
     *
     * More than one @Deployment method is allowed.
     */
    @Deployment(testable = false)
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class).addClasses(GeolocationController.class, ImageController.class, RootObjectTransactions.class);
    }
    
    /**
     * This URL will contain the following URL data
     *
     *  - http://<host>:<port>/<webapp>/
     *
     * This allows the test itself to be agnostic of server information or even
     * the name of the webapp
     *
     */
    @ArquillianResource
    private URL webappUrl;

    @Test
    public void getTransactionsTest() throws Exception
    {
        final WebClient webClient = WebClient.create(webappUrl.toURI());          
        int port = webClient.getCurrentURI().getPort();
        Set<String> imagesUrl = new HashSet();        
        imagesUrl.add("http://localhost:" + port + "/mockapi/img/test1.png");
        imagesUrl.add("http://localhost:" + port + "/mockapi/img/test2.png");
        imagesUrl.add("http://localhost:" + port + "/mockapi/img/test3.png");        
        Set<String> countries = Sets.newHashSet(Randomness.RandomTransactions.countries);
                
        // GET TRANSACTIONS           
        Response res = webClient.path("/mockapi.svc/transactions").query("size", 20).get();
        assertEquals(200, res.getStatus());
        
        final RootObjectTransactions transactions = mapper.readValue((InputStream) res.getEntity(), RootObjectTransactions.class);
        assertEquals(20, transactions.transactions.size());

        List<Transaction> list = transactions.transactions;
        list.forEach((item) -> {
            boolean isDebitOrCredit;
            isDebitOrCredit = "debit".equals(item.type) || "credit".equals(item.type) ? true : false;
            Assert.assertEquals(true, isDebitOrCredit);
            Assert.assertTrue(imagesUrl.contains(item.icon));
            Assert.assertTrue(countries.contains(item.country));
        });
    }
    
    @Test
    public void getNearbyLocationsSizeTest() throws Exception {

        // GET random geolocations
        {
            final WebClient webClient = WebClient.create(webappUrl.toURI());                
            Response res = webClient.path("/mockapi.svc/nearby").query("latitude", 0.0).query("longitude", 0.0).get();
            assertEquals(200, res.getStatus());
            final RootObjectLatLong latlong = mapper.readValue((InputStream)res.getEntity(), RootObjectLatLong.class);
            assertEquals(10, latlong.latlong.size());
        }
    }    
    
    @Test
    public void getImageTest() throws Exception {

        // GET image
        {
            WebClient webClient = WebClient.create(webappUrl.toURI());                
            Response res = webClient.path("/img/test1.png").get();
            assertEquals(200, res.getStatus());
            InputStream file = (InputStream)res.getEntity();            
            Assert.assertNotNull(file);
            
            webClient = WebClient.create(webappUrl.toURI());
            res = webClient.path("/img/test2.png").get();
            assertEquals(200, res.getStatus());
            file = (InputStream)res.getEntity();            
            Assert.assertNotNull(file);

            webClient = WebClient.create(webappUrl.toURI());
            res = webClient.path("/img/test3.png").get();
            assertEquals(200, res.getStatus());
            file = (InputStream)res.getEntity();            
            Assert.assertNotNull(file);       
        }
    }     
    
}
