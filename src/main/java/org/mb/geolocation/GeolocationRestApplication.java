/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mb.geolocation;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.mb.geolocation.api.providers.RequestFilter;
import org.mb.geolocation.api.providers.ResponseFilter;
import org.mb.geolocation.api.resources.GeolocationController;
import org.mb.geolocation.api.resources.ImageController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Admin
 */
@ApplicationPath("")
public class GeolocationRestApplication extends Application{
    
    private static Logger logger = LoggerFactory.getLogger(GeolocationRestApplication.class.getName());

    @Override
    public Set<Class<?>> getClasses() {
        
        Set<Class<?>> set = new HashSet<>();
        
        logger.info("Adding GeolocationController class to rest application", set);
        set.add(GeolocationController.class);
        logger.info("Adding ImageController class to rest application", set);
        set.add(ImageController.class);                
        logger.info("Adding RequestFilter class to rest application", set);
        set.add(RequestFilter.class);
        logger.info("Adding ResponseFilter class to rest application", set);
        set.add(ResponseFilter.class);
        return set;
    }    
    
}
