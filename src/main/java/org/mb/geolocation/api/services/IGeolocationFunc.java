package org.mb.geolocation.api.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.net.SocketException;
import java.security.NoSuchAlgorithmException;
import javax.ejb.Local;
import javax.ws.rs.core.Response;

/**
 *
 * @author Admin
 */
@Local
public interface IGeolocationFunc
{            
    public Response nearby(double latitude, double longitude) throws JsonProcessingException;
    
    public Response transactions(int size) throws JsonProcessingException, NoSuchAlgorithmException, SocketException;        
    
}
