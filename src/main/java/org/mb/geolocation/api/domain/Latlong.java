/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mb.geolocation.api.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Admin
 */
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Latlong
{    
    private double _lat;
    private double _long;

    public Latlong(double latitude, double longitude) {
        this._lat = latitude;
        this._long = longitude;
    }

    public Latlong() {
    }   
    

    @JsonProperty("lat")
    public double getLat() {
        return _lat;
    }

    @JsonProperty("lat")
    public void setLat(double _lat) {
        this._lat = _lat;
    }

    @JsonProperty("long")
    public double getLong() {
        return _long;
    }

    @JsonProperty("long")
    public void setLong(double _long) {
        this._long = _long;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + (int) (Double.doubleToLongBits(this._lat) ^ (Double.doubleToLongBits(this._lat) >>> 32));
        hash = 79 * hash + (int) (Double.doubleToLongBits(this._long) ^ (Double.doubleToLongBits(this._long) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Latlong other = (Latlong) obj;
        return this._lat == other.getLat() && this._long == other.getLong();
    }

}