/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mb.geolocation.api.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 *
 * @author ic
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Transaction {    
    
    public String date;
    public String type;
    public String store_name;
    public String store_address;
    public String country;
    public double amount;
    public String icon;

    public Transaction(String date, String type, String store_name, String store_address, String country, double amount, String icon) {
        this.date = date;
        this.type = type;
        this.store_name = store_name;
        this.store_address = store_address;
        this.country = country;
        this.amount = amount;
        this.icon = icon;
    }

    public Transaction() {

    }      
}
