/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mb.geolocation.api.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.List;

/**
 *
 * @author Admin
 */
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class RootObjectTransactions {
     public List<Transaction> transactions;   
}
