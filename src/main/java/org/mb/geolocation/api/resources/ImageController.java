/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mb.geolocation.api.resources;

import com.google.common.io.ByteStreams;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import org.slf4j.Logger;

/**
 *
 * @author Admin
 */
@Path("")
public class ImageController
{    
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(ImageController.class.getName());
    
    @GET
    @Path("/img/{filename}")
    public Response getImage(@Context HttpServletRequest request, @PathParam(value = "filename") String filename ) throws IOException
    {
        if(filename.isEmpty() || filename == null){
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        // load resource file
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream is = classLoader.getResourceAsStream("images/"+filename);         
        byte[] data = ByteStreams.toByteArray(is);
        String temp = filename;
        StreamingOutput fileStream = (java.io.OutputStream output) -> {
            output.write(data);
            output.flush();
            logger.info("Writing file to output stream... " + temp);
        };
        // try encode filename
        String servedFilename = URLEncoder.encode(filename, "UTF-8");
        logger.info("Downloading file... " + temp);
        return Response.ok(fileStream, MediaType.APPLICATION_OCTET_STREAM).header("Content-Disposition", "attachment; filename=" + servedFilename).build();        
        
    }    
    
}
