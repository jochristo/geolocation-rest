/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mb.geolocation.api.resources;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import javax.ejb.LocalBean;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.mb.geolocation.api.Endpoints;
import org.mb.geolocation.api.domain.Latlong;
import org.mb.geolocation.api.domain.RootObjectLatLong;
import org.mb.geolocation.api.domain.RootObjectTransactions;
import org.mb.geolocation.api.domain.Transaction;
import org.mb.geolocation.api.services.IGeolocationFunc;
import org.mb.geolocation.core.CharsGenerator;
import org.mb.geolocation.core.DatetimeType;
import org.mb.geolocation.core.Distance;
import org.mb.geolocation.core.NumberGenerator;
import org.mb.geolocation.core.RandomDateTime;
import org.mb.geolocation.core.Randomness.RandomTransactions;
import org.slf4j.Logger;

/**
 *
 * @author ic
 */
@Path("/mockapi.svc/")
@LocalBean
public class GeolocationController implements IGeolocationFunc
{    
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(GeolocationController.class.getName());
    private final double radiusDouble = 2000;
    private final int radiusInt = 2000; 
    
    @Context private HttpServletRequest request;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path(Endpoints.NEAR_BY)
    @Override
    public Response nearby(@QueryParam("latitude") double latitude, @QueryParam("longitude") double longitude) throws JsonProcessingException
    {                
        List<Latlong> list = new ArrayList();
        int i = 10;
        HashSet<Latlong> set = new HashSet<Latlong>();

        for (int k = 0; k < i; k++) {
            //Latlong item = Distance.getLocation(longitude, latitude, 2000, new Random());
            Latlong item = Distance.getLocation(longitude, latitude, radiusInt);
            while (set.contains(item)) {
                item = Distance.getLocation(longitude, latitude, radiusInt);
            }
            set.add(item);
        }       
        list.addAll(set);
        RootObjectLatLong rootObject = new RootObjectLatLong();
        rootObject.latlong = list;
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(rootObject);
        return Response.ok().entity(json).build();
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path(Endpoints.TRANSACTIONS)
    @Override
    public Response transactions(@QueryParam("size") int size) throws JsonProcessingException, NoSuchAlgorithmException, SocketException
    {
        int i = size;
        HashSet<Transaction> set = new HashSet();
        RandomDateTime randomDateTime = new RandomDateTime();
        NumberGenerator numberGenerator = new NumberGenerator();
        CharsGenerator charsGenerator = new CharsGenerator();
        Random random = new Random();        
        for (int k = 0; k < i; k++)
        {
            Transaction tr = new Transaction();
            tr.date = randomDateTime.NextToString(DatetimeType.Date);
            tr.type = RandomTransactions.types[numberGenerator.NextInteger(0, 2)];
            tr.country = RandomTransactions.countries[numberGenerator.NextInteger(0, RandomTransactions.countries.length)];
            double d1 = 1500.00 + 1.00;
            double d2 = 1.2;
            double amount = Math.round(random.nextDouble() * d1 + d2);
            tr.amount = amount;
            tr.icon = getRandomImage(request, numberGenerator.NextInteger(0, 3));
            tr.store_name = charsGenerator.getRandomChars(numberGenerator.NextInteger(5, 20));
            tr.store_address = charsGenerator.getRandomChars(numberGenerator.NextInteger(10, 30));
            set.add(tr);
        }
        List<Transaction> list = new ArrayList();
        list.addAll(set);
        RootObjectTransactions rootObject = new RootObjectTransactions();
        rootObject.transactions = list;
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(rootObject);
        return Response.ok().entity(json).build();
    }    
    
    protected String getRandomImage(HttpServletRequest httpServletRequest, int index) throws SocketException
    {
        String[] icons = 
        {
            "http://10.0.1.134:8080/mockapi/img/test1.png",
            "http://10.0.1.134:8080/mockapi/img/test2.png",
            "http://10.0.1.134:8080/mockapi/img/test3.png"
        };   
        String[] filenames = 
        {
            "test1.png",
            "test2.png",
            "test3.png"
        };         
        
        String scheme = httpServletRequest.getScheme();
        
        String host = "localhost" + ":" + httpServletRequest.getLocalPort();        
        String endpoint = "/mockapi/img/";
        StringBuilder url = new StringBuilder(scheme);
        String filename = url.append("://").append(host).append(endpoint).append(filenames[index]).toString();            
        return filename;
    }
    
    /**
     * Finds a local, non-loopback, IPv4 address
     * 
     * @return The first non-loopback IPv4 address found, or
     *         <code>null</code> if no such addresses found
     * @throws SocketException
     *            If there was a problem querying the network
     *            interfaces
     */
    public static InetAddress getLocalAddress() throws SocketException
    {
      Enumeration<NetworkInterface> ifaces = NetworkInterface.getNetworkInterfaces();
      while( ifaces.hasMoreElements() )
      {
        NetworkInterface iface = ifaces.nextElement();
        Enumeration<InetAddress> addresses = iface.getInetAddresses();
        
        while( addresses.hasMoreElements() )
        {
          InetAddress addr = addresses.nextElement();          
          if( addr instanceof Inet4Address && !addr.isLoopbackAddress() )
          {
            return addr;
          }
        }
      }

      return null;
    }     

}
