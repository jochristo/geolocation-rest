/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mb.geolocation.api.providers;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Admin
 */
@Provider
public class RequestFilter implements ContainerRequestFilter
{
    private static final Logger logger = LoggerFactory.getLogger(RequestFilter.class); 
    @Context HttpServletRequest httpServletRequest;
    
    @Override
    public void filter(ContainerRequestContext context) throws IOException {        
        String queryString = httpServletRequest.getQueryString();
        MultivaluedMap<String, String> queryParams = context.getUriInfo().getQueryParameters();
        final StringBuilder logMessage = new StringBuilder("REST Request started: ")
            .append("HTTP METHOD: [")
            .append(context.getRequest().getMethod())
            .append("] PATH INFO: [")
            .append(context.getUriInfo().getAbsolutePath().toString());
            if(httpServletRequest.getQueryString() != null){
                logMessage.append("?"+queryString)
                .append("] REQUEST PARAMETERS: ")
                .append(queryParams);
            }
        logger.info(logMessage.toString());
        
     
    }
    
}
