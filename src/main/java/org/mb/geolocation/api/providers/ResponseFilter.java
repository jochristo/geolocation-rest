/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mb.geolocation.api.providers;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ic
 */
@Provider
public class ResponseFilter implements ContainerResponseFilter
{
    private static final Logger logger = LoggerFactory.getLogger(ResponseFilter.class); 
    @Context HttpServletRequest httpServletRequest;
    
    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        
        MultivaluedMap<String, String> queryParams = requestContext.getUriInfo().getQueryParameters();
        String queryString = httpServletRequest.getQueryString();
        final StringBuilder logMessage = new StringBuilder("Request ended: ")
            .append("HTTP METHOD: [")
            .append(requestContext.getRequest().getMethod())
            .append("] PATH INFO: [")
            .append(requestContext.getUriInfo().getAbsolutePath().toString());
            if(httpServletRequest.getQueryString() != null){
                logMessage.append("?"+queryString)
                .append("] REQUEST PARAMETERS: ")
                .append(queryParams);
            }
            logMessage.append(" with HTTP status ")    
            .append(responseContext.getStatus());
        logger.info(logMessage.toString());
        
        
    }
    
}
