/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mb.geolocation.api;

/**
 *
 * @author Admin
 */
public interface Endpoints {
    
    public final String NEAR_BY = "/nearby"; //?latitude={latitude}&longtitude={longitude}";
    
    public final String TRANSACTIONS = "/transactions"; //?size={size}";
    
    public final String GEOLOCATION = "/geoloc"; //?latitude={latitude}&longtitude={longitude}";
}
