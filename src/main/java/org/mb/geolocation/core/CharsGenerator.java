/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mb.geolocation.core;

import java.security.NoSuchAlgorithmException;
import org.apache.commons.lang.RandomStringUtils;

/**
 *
 * @author Admin
 */
public class CharsGenerator
{
    public static char[] allchars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._-".toCharArray();
    NumberGenerator generator = new NumberGenerator();
    
    public String getRandomChars(int maxSize) throws NoSuchAlgorithmException
    {
        return RandomStringUtils.randomAlphanumeric(maxSize);
    }
    
}
