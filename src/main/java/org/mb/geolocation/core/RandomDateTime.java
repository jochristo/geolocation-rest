/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mb.geolocation.core;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Duration;
import org.joda.time.Hours;
import org.joda.time.LocalDate;
import org.joda.time.Minutes;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author Admin
 */
    public class RandomDateTime
    {
        
        private DateTime start;
        private Random generator = new Random();
        private int range;
        private static String dateTimeFormat = "yyyy-MM-dd HH:mm:ss";
        private static String dateFormat = "dd-MM-yyyy";
        DateTimeFormatter fmt = DateTimeFormat.forPattern(dateFormat);
        DateFormat dateFormatter = new SimpleDateFormat(dateFormat);
        DateFormat dateTimeFormatter = new SimpleDateFormat(dateTimeFormat);
        
        public LocalDate getRandomDate()
        {
            LocalDate startDate = LocalDate.parse("01-01-1995"); //start date
            LocalDate endDate = LocalDate.now(); //end date
            int days = Days.daysBetween(startDate, endDate).getDays();
            LocalDate randomDate = startDate.plusDays((ThreadLocalRandom.current().nextInt(days+1)));
            return randomDate;
        }
        
        public DateTime getRandomDateTime()
        {
            Random random = new Random();

            DateTime startTime = new DateTime(random.nextLong()).withMillisOfSecond(0);

            Minutes minimumPeriod = Minutes.TWO;
            int minimumPeriodInSeconds = minimumPeriod.toStandardSeconds().getSeconds();
            int maximumPeriodInSeconds = Hours.ONE.toStandardSeconds().getSeconds();

            Seconds randomPeriod = Seconds.seconds(random.nextInt(maximumPeriodInSeconds - minimumPeriodInSeconds));
            DateTime endTime = startTime.plus(minimumPeriod).plus(randomPeriod);

            DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(dateFormat);     
            return dateTimeFormatter.parseDateTime(endTime.toString(dateTimeFormatter));
        }
        
        public DateTime GenererateRandomDate(int lowYear, int highYear)
        {
            //var random = new Random();
            int year = generator.nextInt(highYear-lowYear) + lowYear;
            int month = generator.nextInt(12-1) + 1;
            LocalDate ld = new LocalDate(year, month, 1);
            int day = ld.dayOfMonth().getMaximumValue();
            int Day = generator.nextInt(day-1) + 1;
            DateTime dt = new DateTime(year, month, Day, 8, 00, 00);
            return dt;
        }      
        
        private DateTime NextDateTime(int year, int month, int day)
        {
            start = new DateTime(year, month, day, 0 ,00, 00);
            Duration duration = new Duration(DateTime.now(), start);
            Long arange = duration.getStandardDays();            
            return GenererateRandomDate(start.getYear(), DateTime.now().getYear());
        }        
        
        private DateTime NextDate()
        {
            this.start = DateTime.now();
            DateTime high = start.plusYears(-18); // start at 18 yrs old
            DateTime low = high.plusYears(-32); // go as back as 50 yrs max                        
            return GenererateRandomDate(low.getYear(), high.getYear());
        }  
        
        public String NextToString(DatetimeType datetimeType)
        {
            String type = datetimeType == DatetimeType.DateTime ? dateTimeFormat : dateFormat;
            return type = datetimeType == DatetimeType.DateTime ? fmt.print(NextDateTime(1990, 1, 1)) : fmt.print(NextDate());
        }        
        
        
    }