/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mb.geolocation.core;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Random;

/**
 *
 * @author Admin
 */
public class NumberGenerator {
    
        private Random random = new Random();

        public int NextInteger(int low, int high)
        {
            return random.nextInt(high-low)+low;
        }

        public String NextDouble(int lowerBound, int upperBound, int decimalPlaces)
        {
            if (upperBound <= lowerBound || decimalPlaces < 0)
            {
                throw new IllegalArgumentException("Error occured");
            }
            double value = ((random == null ? new Random() : random).nextDouble() * (upperBound - lowerBound)) + lowerBound;
            //String formatted = value.ToString("N7", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
            NumberFormat formatter = new DecimalFormat("#0.000000");
            String formatted = formatter.format(value); // earlier java 
            formatted = String.format(Locale.ROOT, "%.6f", value); // java 8            
            return formatted;
        }    
}
