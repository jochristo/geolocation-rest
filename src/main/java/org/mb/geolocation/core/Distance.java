/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mb.geolocation.core;

import java.util.Locale;
import java.util.Random;
import org.mb.geolocation.api.domain.Latlong;

/**
 *
 * @author Admin
 */
public class Distance
{       
    
    public static double Calculate(double lat1, double long1, double lat2, double long2) {
        double R = 6371;
        double dLat = ToRad((lat2 - lat1));
        double dLon = ToRad((long2 - long1));
        lat1 = ToRad(lat1);
        lat2 = ToRad(lat2);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = R * c;
        return d;
    }

    public static double ToRad(double value) {
        return value * Math.PI / 180;
    }

    public static double distance(double lat1, double lon1, double lat2, double lon2, char unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(ToRad(lat1)) * Math.sin(ToRad(lat2)) + Math.cos(ToRad(lat1)) * Math.cos(ToRad(lat2)) * Math.cos(ToRad(theta));
        dist = Math.acos(dist);
        dist = ToRad(dist);
        dist = dist * 60 * 1.1515;
        if (unit == 'K') {
            dist = dist * 1.609344;
        } else if (unit == 'N') {
            dist = dist * 0.8684;
        }
        return (dist);
    }

    public static Latlong getLocation(double x0, double y0, int radius) {
        Random random = new Random();

        // Convert radius from meters to degrees
        double radiusInDegrees = radius / 111000f;

        double u = random.nextDouble();
        double v = random.nextDouble();
        double w = radiusInDegrees * Math.sqrt(u);
        double t = 2 * Math.PI * v;
        double x = w * Math.cos(t);
        double y = w * Math.sin(t);

        // Adjust the x-coordinate for the shrinking of the east-west distances
        //double new_x = x / Math.cos(ToRad(y0));
        double new_x = x / Math.cos(y0);

        double foundLongitude = new_x + x0;
        double foundLatitude = y + y0;

        String longitudeString = String.format(Locale.ROOT, "%.6f", foundLongitude);
        String latitudeString = String.format(Locale.ROOT, "%.6f", foundLatitude);
        return new Latlong(Double.valueOf(latitudeString), Double.valueOf(longitudeString));

    }

    public static Latlong getLocation(double x0, double y0, int radius, Random random) {

        // Convert radius from meters to degrees
        double radiusInDegrees = radius / 111300; // 111000f;

        double u = random.nextDouble();
        double v = random.nextDouble();
        double w = radiusInDegrees * Math.sqrt(u);
        double t = 2 * Math.PI * v;
        double x = w * Math.cos(t);
        double y = w * Math.sin(t);

        // Adjust the x-coordinate for the shrinking of the east-west distances
        double new_x = x / Math.cos(ToRad(y0));

        double foundLongitude = new_x + x0;
        double foundLatitude = y + y0;
        String longitudeString = String.format(Locale.ROOT, "%.6f", foundLongitude);
        String latitudeString = String.format(Locale.ROOT, "%.6f", foundLatitude);
        return new Latlong(Double.valueOf(latitudeString), Double.valueOf(longitudeString));
    }

    public static Latlong getNearByLocation(double x0, double y0, int radius) {
        Random random = new Random();

        // Convert radius from meters to degrees
        double radiusInDegrees = radius / 111300; // 111000f;

        double u = random.nextDouble();
        double v = random.nextDouble();
        double w = radiusInDegrees * Math.sqrt(u);
        double t = 2 * Math.PI * v;
        double x = w * Math.cos(t);
        double y = w * Math.sin(t);

        // Adjust the x-coordinate for the shrinking of the east-west distances
        double new_x = x / Math.cos(ToRad(y0));

        double foundLongitude = new_x + x0;
        double foundLatitude = y + y0;
        String longitudeString = String.format(Locale.ROOT, "%.6f", foundLongitude);
        String latitudeString = String.format(Locale.ROOT, "%.6f", foundLatitude);
        return new Latlong(Double.valueOf(latitudeString), Double.valueOf(longitudeString));
    }   
}
